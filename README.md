# Ansible - Workshop
Lab 05: Roles

---

# Preperations

## Prepare hosts file - open host file

```
$ vim /etc/ansible/hosts
```

## Paste the following snippet in the file - nodeip will be replaced by the managed node ip(or ip's) - <bold>if the hosts file is already updated, skip this step</bold>

```
[demoservers]
(nodeip(s))
```

## Save and quit
```
press esc
press :wq
```

# Instructions

- Download geerlingguy.apache role
- Define playbook to run the role
- Run the playbook
- Navigate to port 80 to validate the playbook
---

## Download geerlingguy.apache role
```
$ ansible-galaxy install geerlingguy.apache
```

## Define playbook to run the role

### Create a new playbook file
```
$ sudo vim apacheplaybook.yml
press i
```

### Paste the following snippet
```
---
- hosts: localhost
  become: true
  roles:
    - role: geerlingguy.apache
```

### Save and quit
```
press esc
press :wq
```

## Run the playbook

```
$ ansible-playbook apacheplaybook.yml
```

## Navigate to port 80 of localhost to see the nginx home page

```
http://(control-machine-ip):80
```



